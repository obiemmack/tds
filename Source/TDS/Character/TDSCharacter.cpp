// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GamePlayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TDS/Game/TDSGameInstance.h"
#include "PhysicalMaterials/PhysicalMaterial.h"


ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
		
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDSCharacter::InitWeapon);
	}

}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	myRotationVector = FVector(AxisX, AxisY, 0.0f);
	

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
	StaminaFunc();
}
void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("Sprint"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputSprintPressed);
	NewInputComponent->BindAction(TEXT("Walk"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputWalkPressed);
	NewInputComponent->BindAction(TEXT("Aim"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAimPressed);
	NewInputComponent->BindAction(TEXT("Sprint"), EInputEvent::IE_Released, this, &ATDSCharacter::InputSprintReleased);
	NewInputComponent->BindAction(TEXT("Walk"), EInputEvent::IE_Released, this, &ATDSCharacter::InputWalkReleased);
	NewInputComponent->BindAction(TEXT("Aim"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAimReleased);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("NextWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwitchNextWeapon);
	NewInputComponent->BindAction(TEXT("PrevWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwitchPreviosWeapon);

}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}
void ATDSCharacter::InputWalkPressed()
{
	WalkEnabled = true;
	ChangeMovementState();
	
}

void ATDSCharacter::InputWalkReleased()
{
	WalkEnabled = false;
	ChangeMovementState();
	
}

void ATDSCharacter::InputSprintPressed()
{
	SprintEnabled = true;
	ChangeMovementState();
	
}

void ATDSCharacter::InputSprintReleased()
{
	FRotator CurentRot = GetActorRotation();
	SprintEnabled = false;
	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController)
	{
		FHitResult HitResult;
		myController->GetHitResultUnderCursorByChannel(TraceTypeQuery6, false, HitResult);
		FRotator CursorRot = FRotator(0.0f, UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw, 0.0f);
		SetActorRotation(FQuat::Slerp((FQuat(CurentRot)), (FQuat(CursorRot)), 5.0f));
					
	}
	
	ChangeMovementState();
}

void ATDSCharacter::InputAimPressed()
{
	AimEnabled = true;
	ChangeMovementState();
	
}

void ATDSCharacter::InputAimReleased()
{
	AimEnabled = false;
	ChangeMovementState();
}

void ATDSCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATDSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);
	FVector myRotVec = FVector(AxisX, AxisY, 0.0f);
	FRotator LastRot = myRotVec.ToOrientationRotator();
	
	
	
	if (MovementState == EMovementState::Sprint_State)
	{		
		FRotator DirectionRot = myRotationVector.ToOrientationRotator();
		FRotator CurentRot = GetActorRotation();
		if (myRotationVector != FVector(0.0f, 0.0f, 0.0f))
		{
			SetActorRotation(FQuat::Slerp((FQuat(CurentRot)), (FQuat(DirectionRot)), DeltaTime));
		}
	}
	else
	{
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FRotator CurentRot = GetActorRotation();
			FHitResult HitResult;
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, HitResult);
			FRotator CursorRot = FRotator(0.0f, UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw, 0.0f);
			SetActorRotation(FQuat(FRotator(0.0f, UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw, 0.0f)));
			
			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::AimWalk_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Sprint_State:
					break;
				default:
					break;
				}
				CurrentWeapon->ShootEndLocation = HitResult.Location + Displacement;
			}
		}
	}
		
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ATDSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		if (!CurrentWeapon->WeaponReloading)
		{
			if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound)
				CurrentWeapon->InitReload();
		}
	}
}

void ATDSCharacter::ChangeMovementState()
{
	if (!WalkEnabled && !AimEnabled && !SprintEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		
		if (SprintEnabled && CanRun)
		{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::Sprint_State;
		}
		if (SprintEnabled && !CanRun)
		{
			MovementState = EMovementState::Run_State;
		}
		if (WalkEnabled && AimEnabled && !SprintEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !AimEnabled && !SprintEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && AimEnabled && !SprintEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}
	CharacterUpdate();
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void ATDSCharacter::StaminaFunc()
{
	
	if (!SprintEnabled && Stamina<StaminaMax)
	{
		++Stamina;
	}
	if (SprintEnabled && Stamina < StaminaMax && myRotationVector == FVector(0.0f, 0.0f, 0.0f))
	{
		++Stamina;
	}
	if (SprintEnabled && Stamina>0 && myRotationVector != FVector(0.0f, 0.0f, 0.0f))
	{
		--Stamina;
	}

	if (Stamina == 0)
	{
		CanRun = false;
		
		ChangeMovementState();
		
	}
	if (Stamina > StaminaMin)
	{
		CanRun = true;
	}
}

AWeaponDefault* ATDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDSCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}


	UTDSGameInstance* myGi = Cast <UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGi)
	{
		if(myGi->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSoCketRightHand"));
					CurrentWeapon = myWeapon;
					
					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->bIsPistol = myWeaponInfo.bIsPistol;
					myWeapon->UpdateStateWeapon(MovementState);
					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					if (InventoryComponent)
					{
						CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(IdWeaponName);
					}

					
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFire.AddDynamic(this, &ATDSCharacter::WeaponFire);
				}
			}
		
		}
		else
			{
				UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter:: WeaponInit - Weapon not find in table"));
			}
	}
	
}

UDecalComponent* ATDSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}




void ATDSCharacter::WeaponReloadStart(UAnimMontage* CharAnim, UAnimMontage* WeaponAnim)
{
	if (WeaponAnim && CurrentWeapon->SkeletalMeshWeapon->GetAnimInstance())
	{
		CurrentWeapon->SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponAnim);
	}
	else
	{
		UE_LOG(LogTemp, Warning, (TEXT("ATDSCharacter::No AnimInstance or AnimMontage")));
	}
	WeaponReloadStart_BP(CharAnim);
}

void ATDSCharacter::WeaponReloadEnd(bool bIsSuccess)
{
	WeaponReloadEnd_BP(bIsSuccess);
}



void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* CharAnim)
{
}
void ATDSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
}

void ATDSCharacter::WeaponFire(UAnimMontage* CharAnim, UAnimMontage* WeaponAnim)
{
	if (WeaponAnim && CurrentWeapon->SkeletalMeshWeapon->GetAnimInstance())
	{
		CurrentWeapon->SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponAnim);
	}
	else
	{
		UE_LOG(LogTemp, Warning, (TEXT("ATDSCharacter::No AnimInstance or AnimMontage")));
	}
	WeaponFire_BP(CharAnim);
}

void ATDSCharacter::WeaponFire_BP_Implementation(UAnimMontage* CharAnim)
{	
}

void ATDSCharacter::TrySwitchNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->ReloadCancel();
			}

		}


		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo))
			{ }
		}
	}
}

void ATDSCharacter::TrySwitchPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			/*if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}*/

		}


		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo))
			{
			}
		}
	}
}






