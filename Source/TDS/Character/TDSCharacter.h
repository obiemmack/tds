// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDS/Weapon/WeaponDefault.h" 
#include "TDS/Weapon/InventoryComponent.h"
#include "TDSCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

public:
	ATDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;
public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Speed")
	//Movement
		
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Speed")
		FCharacterSpeed MovementInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Speed")
		bool SprintEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Speed")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Speed")
		bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Speed")
		FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
	//Stamina
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		int Stamina = StaminaMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		int StaminaMax = 500;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		int StaminaMin = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		bool CanRun = true;
	//Weapon	
	AWeaponDefault* CurrentWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	UDecalComponent* CurrentCursor = nullptr;

	//Inventory
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory Component", meta = (AllowPrivateAccess = "true"))
	class UInventoryComponent* InventoryComponent;
	int32 CurrentIndexWeapon = 0;


	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);

	void InputWalkPressed();
	void InputWalkReleased();

	void InputSprintPressed();
	void InputSprintReleased();

	void InputAimPressed();
	void InputAimReleased();

	void InputAttackPressed();
	void InputAttackReleased();
	
	float AxisX = 0.0f;
	float AxisY = 0.0f;
	float RotMix;

	


	//Tick Func
	UFUNCTION()
	void MovementTick(float DeltaTime);
	//Func
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	UFUNCTION(BlueprintCallable)
		void StaminaFunc();
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo);
	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* CharAnim, UAnimMontage* WeaponAnim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* CharAnim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	UFUNCTION()
		void WeaponFire(UAnimMontage* CharAnim, UAnimMontage* WeaponAnim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFire_BP(UAnimMontage* CharAnim);
	
	//InventoryFunc
	void TrySwitchNextWeapon();
	void TrySwitchPreviosWeapon();

};

