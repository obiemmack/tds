// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 ShowDebugExplose = 0;

FAutoConsoleVariableRef CVarShowExploseDebug(TEXT("ExploseDebug"), ShowDebugExplose, TEXT("Draw Debug Explose"), ECVF_Cheat);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
	
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Timer(DeltaTime);


}

void AProjectileDefault_Grenade::Timer(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerExplose > TimeExplose)
		{
			Explose();
			
		}
		else
		{
			TimerExplose += DeltaTime;
		}
	}
	
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
	
}

void AProjectileDefault_Grenade::Explose()
{
	FVector MyLocation = GetActorLocation();
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray <AActor*> IgnoredActors;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseDamage,	ProjectileSetting.ExploseDamage * 0.2f,
		GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, ProjectileSetting.ProjectileMaxRadiusDamage, 5, NULL, IgnoredActors, this, nullptr,ECC_Visibility);
	
	

	TArray <FHitResult> OutHits;
	
	FCollisionShape CollisionSphere = FCollisionShape::MakeSphere(ProjectileSetting.ProjectileMaxRadiusDamage);
	bool isHit = GetWorld()->SweepMultiByChannel(OutHits, MyLocation, MyLocation+FVector(0.01f, 0.01f, 0.01f), FQuat::Identity, ECC_Visibility, CollisionSphere);
	if (isHit)
	{
		for (auto& Hit : OutHits) 
		{
			UStaticMeshComponent* MeshComp = Cast<UStaticMeshComponent>((Hit.GetActor())->GetRootComponent());
			
			
				if (MeshComp)
				{
					
					MeshComp->AddRadialImpulse(MyLocation, ProjectileSetting.ProjectileMaxRadiusDamage, ProjectileSetting.ExploseImpulsePower, ERadialImpulseFalloff::RIF_Constant, true);
					
										
				};
		}

		if (ShowDebugExplose)
		{
			DrawDebugSphere(GetWorld(), MyLocation, ProjectileSetting.ProjectileMinRadiusDamage, 8, FColor::Red, false, 5.f, (uint8)'\000', 1.f);
			DrawDebugSphere(GetWorld(), MyLocation, (ProjectileSetting.ProjectileMaxRadiusDamage + ProjectileSetting.ProjectileMinRadiusDamage) / 2, 8, FColor::Orange, false, 5.f, (uint8)'\000', 1.f);
			DrawDebugSphere(GetWorld(), MyLocation, ProjectileSetting.ProjectileMaxRadiusDamage, 8, FColor::Yellow, false, 5.f, (uint8)'\000', 1.f);
		}
	}


	this->Destroy();
}
