// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Weapon/InventoryComponent.h"
#include "TDS/Game/TDSGameInstance.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	for (int8 i = 0; i < WeaponSlots.Num(); i++)
	{
		UTDSGameInstance* myGi = Cast<UTDSGameInstance>(GetWorld() ->GetGameInstance() );
		if (myGi)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				FWeaponInfo Info;
				if (myGi->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
				{
					WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;
				}
				else
				{
					WeaponSlots.RemoveAt(i);
					i--;
				}
			}
		}
	}

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
		{
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo);
		}
	}
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num() - 1)
	{
		CorrectIndex = 0;
	}
	else
	{
		if (ChangeToIndex < 0)
		{
			CorrectIndex = WeaponSlots.Num() - 1;
		}
	}

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;

	int8 i = 0;

	while (i < WeaponSlots.Num() && !bIsSuccess)
	{
		if (WeaponSlots[i].IndexSlot == CorrectIndex)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				NewIdWeapon = WeaponSlots[i].NameItem;
				NewAdditionalInfo = WeaponSlots[i].AdditionalInfo;
				bIsSuccess = true;
			}
		}
		i++;
	}

	if (!bIsSuccess)
	{

	}

	if (bIsSuccess)
	{
		SetAdditionalWeaponInfo(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo);

	}

	return bIsSuccess;
}

FAdditionalWeaponInfo UInventoryComponent::GetAdditionalWeaponInfo(int32 IndexWeapon)
{
	FAdditionalWeaponInfo Result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;

		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (WeaponSlots[i].IndexSlot == IndexWeapon)
			{
				Result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("AInventoryComponent::GetAdditionalWeaponInfo - Not Found Weapon With Index  - d%"), IndexWeapon);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AInventoryComponent::GetAdditionalWeaponInfo - Not Correct IndexWeapon  - d%"), IndexWeapon);
	}


	return Result;
}

int32 UInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 Result = -1;

	int8 i = 0;
	bool bIsFind = false;

	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			Result = WeaponSlots[i].IndexSlot;
			bIsFind = true;
		}
		i++;
	}

	return Result;
}

void UInventoryComponent::SetAdditionalWeaponInfo(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		int8 i = 0;
		bool bIsFind = false;

		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (WeaponSlots[i].IndexSlot == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("AInventoryComponent::SetAdditionalWeaponInfo - Not Found Weapon With Index  - d%"), IndexWeapon);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AInventoryComponent::SetAdditionalWeaponInfo - Not Correct IndexWeapon  - d%"), IndexWeapon);
	}
}

