// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault.h"
#include "Kismet/GamePlayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	BulletCollisionSphere->SetSphereRadius(16.f);

	

	BulletCollisionSphere->bReturnMaterialOnMove = true;//hit event return physMaterial

	BulletCollisionSphere->SetCanEverAffectNavigation(false);//collision not affect navigation (P keybord on editor)

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);
	

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 1.f;
	BulletProjectileMovement->MaxSpeed = 0.f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();
	
	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);
	
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	DamageFalloffTimer(DeltaTime);
}

void AProjectileDefault::DamageFalloffTimer(float DeltaTime)
{
	if (FinalDamage > ProjectileSetting.ProjectileMinDamage)
	{


		if (FalloffTimer <= ProjectileSetting.TimeToStartDMGFalloff)
		{
			FalloffTimer +=DeltaTime;
		}
		else
		{
			FinalDamage = ProjectileSetting.ProjectileMaxDamage--;
		}

	}
	else
	{
		FinalDamage = ProjectileSetting.ProjectileMinDamage;
	}
}

void AProjectileDefault::InitProjectile(FProjectileInfo InitParam, FWeaponInfo WeaponInfo)
{
	if (InitParam.BulletMesh)
	{
		BulletMesh->SetStaticMesh(InitParam.BulletMesh);
	}
	else
	{
		BulletMesh->DestroyComponent();
	}
	if (InitParam.BulletFX)
	{
		BulletFX->SetTemplate(InitParam.BulletFX);
	}
	else
	{
		BulletFX->DestroyComponent();
	}

	BulletProjectileMovement->InitialSpeed = InitParam.ProjectileIntSpeed;
	BulletProjectileMovement->MaxSpeed = InitParam.ProjectileIntSpeed;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);
	ProjectileSetting = InitParam;
	WeaponSetting = WeaponInfo;
	FinalDamage = ProjectileSetting.ProjectileMaxDamage;
}

void AProjectileDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);
		if (WeaponSetting.HitDecals.Contains(mySurfacetype))
		{
			
			UMaterialInterface* myMaterial = WeaponSetting.HitDecals[mySurfacetype];
			if (myMaterial && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation() + FRotator(0.0f, 0.0f, UKismetMathLibrary::RandomFloatInRange(0.0f, 360.0f)), EAttachLocation::KeepWorldPosition, 10.0f);
			}
		}
		if (WeaponSetting.HitFX.Contains(mySurfacetype))
		{
			UParticleSystem* myParticle = WeaponSetting.HitFX[mySurfacetype];
			if (myParticle) 
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
			}
		}
		if (WeaponSetting.HitSound)
		{
			
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.HitSound, Hit.ImpactPoint);
		}
	}
	
	UGameplayStatics::ApplyDamage(OtherActor, FinalDamage, GetInstigatorController(), this, NULL);
	ImpactProjectile();
	
}

void AProjectileDefault::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AProjectileDefault::BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileDefault::ImpactProjectile()
{
	
	this->Destroy();
}

