// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"


int32 ShowDebugWeapon = 0;

FAutoConsoleVariableRef CVarShowWeaponDebug(TEXT("WeaponDebug"), ShowDebugWeapon, TEXT("Draw Debug Weapon"), ECVF_Cheat);



// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	WeaponInit();
	FireTimer = 0.01f;
	
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	DropClipTick(DeltaTime);
	DropShellTick(DeltaTime);
	
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (GetWeaponRound() > 0)
	{
		//if (WeaponFiring)
			if (FireTimer < 0.0f)
			{
				if (!WeaponReloading && WeaponFiring)
					Fire();
			}
			else
			FireTimer -= DeltaTime;
			
	}
	else
	{
			if (!WeaponReloading)
			{
				InitReload();
			}
	}
			
		
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			ReloadFinish();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading) 
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}
		if (CurrentDispersion > CurrentDispersionMax)
		{
			CurrentDispersion = CurrentDispersionMax;
		}
		else
		{
			if (CurrentDispersion < CurrentDispersionMin)
				CurrentDispersion = CurrentDispersionMin;
		}
	
	}
}

void AWeaponDefault::DropClipTick(float DeltaTime)
{
	if (DropClipFlag)
	{
		if (DropClipTimer < 0.0f)
		{
			DropClipFlag = false;
			InitDropMesh(WeaponSetting.ClipDropMesh.DropMesh, WeaponSetting.ClipDropMesh.DropMeshOffset,
				WeaponSetting.ClipDropMesh.DropMeshImpulseDir, WeaponSetting.ClipDropMesh.DropMeshLifeTime,
				WeaponSetting.ClipDropMesh.ImpulseDispersion, WeaponSetting.ClipDropMesh.ImpulsePower,
				WeaponSetting.ClipDropMesh.CustomMass);
		}
		else
			DropClipTimer -= DeltaTime;
	}
}

void AWeaponDefault::DropShellTick(float DeltaTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0.0f)
		{
			DropShellFlag = false;
			InitDropMesh(WeaponSetting.ShellDropMesh.DropMesh, WeaponSetting.ShellDropMesh.DropMeshOffset,
				WeaponSetting.ShellDropMesh.DropMeshImpulseDir, WeaponSetting.ShellDropMesh.DropMeshLifeTime,
				WeaponSetting.ShellDropMesh.ImpulseDispersion, WeaponSetting.ShellDropMesh.ImpulsePower,
				WeaponSetting.ShellDropMesh.CustomMass);
		}
		else
			DropShellTimer -= DeltaTime;
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
	ReloadTime = WeaponSetting.ReloadTime;
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
	
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSettings;
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSetting.RateOfFire;
	AdditionalWeaponInfo.Round--;
	ChangeDispersionByShot();

	
	if (WeaponSetting.ShellDropMesh.DropMesh)
	{
		DropShellFlag = true;
		DropShellTimer = WeaponSetting.ShellDropMesh.DropMeshInitTime;
	}
	
	if (WeaponSetting.AnimSettings.AnimCharFire)
	{

		switch (CharMovementState)
		{
		case EMovementState::Aim_State:
			if (WeaponSetting.AnimSettings.AnimCharAimFire)
			{
				OnWeaponFire.Broadcast(WeaponSetting.AnimSettings.AnimCharAimFire,WeaponSetting.AnimSettings.AnimWeaponFire);
			}
			else
			{
				OnWeaponFire.Broadcast(WeaponSetting.AnimSettings.AnimCharFire, WeaponSetting.AnimSettings.AnimWeaponFire);
			}

			break;
		case EMovementState::AimWalk_State:
			if (WeaponSetting.AnimSettings.AnimCharAimFire)
			{
				OnWeaponFire.Broadcast(WeaponSetting.AnimSettings.AnimCharAimFire, WeaponSetting.AnimSettings.AnimWeaponFire);
			}
			else
			{
				OnWeaponFire.Broadcast(WeaponSetting.AnimSettings.AnimCharFire, WeaponSetting.AnimSettings.AnimWeaponFire);
			}
			break;
		case EMovementState::Walk_State:
			OnWeaponFire.Broadcast(WeaponSetting.AnimSettings.AnimCharFire, WeaponSetting.AnimSettings.AnimWeaponFire);
			break;
		case EMovementState::Run_State:
			OnWeaponFire.Broadcast(WeaponSetting.AnimSettings.AnimCharFire, WeaponSetting.AnimSettings.AnimWeaponFire);
			break;
		case EMovementState::Sprint_State:
			break;
		default:
			break;
		}
	}
	
	

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentLocation(),ShootLocation->GetComponentRotation());

	int8 NumberProjectile = GetNumberProjectileByShot();

	

	

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;

		for (int8 i = 0; i < NumberProjectile; i++)
		{
			EndLocation = GetFireEndLocation();
			

			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire

				FVector Dir = EndLocation - SpawnLocation;
				Dir.Normalize();
				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSetting.ProjectileSettings,WeaponSetting);
				}
			}
			else
			{
				//Projectile null Init trace fire
				FHitResult OutHit;
				TArray<AActor*> Actors;				
				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation,
					ETraceTypeQuery::TraceTypeQuery4, false, Actors, EDrawDebugTrace::None, OutHit, true,
					FLinearColor::Green, FLinearColor::Red, 5.0f);
				if (ShowDebugWeapon)
				{
					DrawDebugLine(GetWorld(), SpawnLocation, EndLocation, FColor::Orange, false, 5.f, (uint8)'\000', 2.0f);
				}

				//Damage Falloff
				DamageFalloff =fmaxf(0, (OutHit.Distance - WeaponSetting.MaxDamageDistance) / (WeaponSetting.DistanceTrace - WeaponSetting.MaxDamageDistance));
				FinalDamage = WeaponSetting.WeaponDamage-(WeaponSetting.WeaponDamage-WeaponSetting.MinWeaponDamage) * DamageFalloff;
				UGameplayStatics::ApplyPointDamage(OutHit.GetActor(), FinalDamage, SpawnLocation, OutHit, GetInstigatorController(), this, NULL);
				
								

					if (OutHit.GetActor()&& OutHit.PhysMaterial.IsValid())
					{
						
						EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(OutHit);
						
						
						if (WeaponSetting.HitDecals.Contains(mySurfacetype))
						{
														
							UMaterialInterface* myMaterial = WeaponSetting.HitDecals[mySurfacetype];
							if (myMaterial && OutHit.GetComponent())
							{
								UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), OutHit.GetComponent(), NAME_None, OutHit.ImpactPoint, OutHit.ImpactNormal.Rotation()+FRotator(0.0f, 0.0f, UKismetMathLibrary::RandomFloatInRange(0.0f, 360.0f)), EAttachLocation::KeepWorldPosition, 10.0f);
							}
						}
						if (WeaponSetting.HitFX.Contains(mySurfacetype))
						{
							UParticleSystem* myParticle = WeaponSetting.HitFX[mySurfacetype];
							if (myParticle)
							{
								UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(OutHit.ImpactNormal.Rotation(), OutHit.ImpactPoint, FVector(1.0f)));
							}
						}
						if (WeaponSetting.HitSound)
						{
							UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.HitSound, OutHit.ImpactPoint);
						}
						if(ShowDebugWeapon)
						{
							GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("You are hitting: %s"), *OutHit.GetActor()->GetName()));

							GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, FString::Printf(TEXT("ImpactPoint: %s"), *OutHit.ImpactPoint.ToString()));

							GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, FString::Printf(TEXT("Impact Normal: %s"), *OutHit.ImpactNormal.ToString()));
						}
						
					}
					
				
			}
		}
		
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	BlockFire = false;
	CharMovementState = NewMovementState;
	switch (CharMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimReduction;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimReduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimReduction;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionAimReduction;
		break;
	case EMovementState::Sprint_State:
		BlockFire = true;
		SetWeaponStateFire(false);
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot,GetCurrentDispersion() * PI/180.0f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);
	
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation)
			.GetSafeNormal()) * -WeaponSetting.DistanceTrace;
		if (ShowDebugWeapon)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation),
				WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald,
				false, 2.1f, (uint8)'\000', 1.0f);
		
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * WeaponSetting.DistanceTrace;
		if (ShowDebugWeapon)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistanceTrace,
				GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		
	}


	if (ShowDebugWeapon)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::White, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Orange, false, 5.f, (uint8)'\000', 0.5f);
		
		
	}


	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.BulletsPerShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;
	ReloadTimer = WeaponSetting.ReloadTime;
	if (WeaponSetting.AnimSettings.AnimCharReload)
	{
		
		switch (CharMovementState)
		{
		case EMovementState::Aim_State:
			if (WeaponSetting.AnimSettings.AnimCharAimReload)
			{
				OnWeaponReloadStart.Broadcast(WeaponSetting.AnimSettings.AnimCharAimReload, WeaponSetting.AnimSettings.AnimWeaponReload);
			}
			else
			{
				OnWeaponReloadStart.Broadcast(WeaponSetting.AnimSettings.AnimCharReload, WeaponSetting.AnimSettings.AnimWeaponReload);
			}
			
			break;
		case EMovementState::AimWalk_State:
			if (WeaponSetting.AnimSettings.AnimCharAimReload)
			{
				OnWeaponReloadStart.Broadcast(WeaponSetting.AnimSettings.AnimCharAimReload, WeaponSetting.AnimSettings.AnimWeaponReload);
			}
			else
			{
				OnWeaponReloadStart.Broadcast(WeaponSetting.AnimSettings.AnimCharReload, WeaponSetting.AnimSettings.AnimWeaponReload);
			}
			break;
		case EMovementState::Walk_State:
			OnWeaponReloadStart.Broadcast(WeaponSetting.AnimSettings.AnimCharReload, WeaponSetting.AnimSettings.AnimWeaponReload);
			break;
		case EMovementState::Run_State:
			OnWeaponReloadStart.Broadcast(WeaponSetting.AnimSettings.AnimCharReload, WeaponSetting.AnimSettings.AnimWeaponReload);
			break;
		case EMovementState::Sprint_State:
			break;
		default:
			break;
		}

		if (WeaponSetting.ClipDropMesh.DropMesh)
		{
			DropClipFlag = true;
			DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshInitTime;
		}
		
	}
} 

void AWeaponDefault::ReloadFinish()
{
	WeaponReloading = false;
	AdditionalWeaponInfo.Round = WeaponSetting.MaxRound;
	OnWeaponReloadEnd.Broadcast(true);
}

void AWeaponDefault::ReloadCancel()
{
	WeaponReloading = false;
	DropClipFlag = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15);
	}
	OnWeaponReloadEnd.Broadcast(false);
}

void AWeaponDefault::InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDir, float LifeTimeMesh, float ImpulseRandDispersion, float PowerImpulse, float CustomMass)
{
	FTransform Transform;
	
	FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() * Offset.GetLocation().Y + this->GetActorUpVector() * Offset.GetLocation().Z;
	Transform.SetLocation(GetActorLocation() + LocalDir);
	Transform.SetScale3D(Offset.GetScale3D());
	Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());

	AStaticMeshActor* NewActor = nullptr;
	FActorSpawnParameters Params;
	Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Params.Owner = this;

	NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(),Transform, Params);
	
	if (NewActor && NewActor->GetStaticMeshComponent())
	{
		NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

		NewActor->SetActorTickEnabled(false);
		NewActor->SetLifeSpan(LifeTimeMesh);

		NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
		NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
		NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

		if (CustomMass > 0.0f)
		{
			NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
		}

		

		NewActor->GetStaticMeshComponent()->AddImpulse((DropImpulseDir + 
			UKismetMathLibrary::RandomUnitVectorInConeInDegrees(GetActorRightVector(), ImpulseRandDispersion))* PowerImpulse);
	}
}

